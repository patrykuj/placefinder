package com.finder.search;

import com.finder.controllers.place.find.PlaceInfo;
import com.finder.errors.AmbiguousCityException;
import com.finder.errors.NoCityFoundException;
import com.finder.search.criteria.ExtendPlaceSearchCriteria;
import com.finder.search.criteria.PlaceSearchCriteria;
import com.finder.services.SearchCityService;
import com.finder.services.SearchPlaceService;
import facebook4j.Place;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchFactory {
    private final static Logger LOGGER = Logger.getLogger(SearchFactory.class);

    private SearchCityService searchCityService;
    private SearchPlaceService searchPlaceService;

    public SearchFactory(SearchCityService searchCityService, SearchPlaceService searchPlaceService) {
        this.searchCityService = searchCityService;
        this.searchPlaceService = searchPlaceService;
    }

    public Place cityDetails(String country, String city, String cityState, String zipCode) throws AmbiguousCityException, NoCityFoundException {
        Place place;
        if(cityState.equalsIgnoreCase("") && zipCode.equalsIgnoreCase("")){
            place = searchCityService.searchCityDetails(new PlaceSearchCriteria(country, city));
        }else{
            place = searchCityService.searchCityDetails(new ExtendPlaceSearchCriteria(country, city, cityState, zipCode));
        }
        return place;
    }

    public List<PlaceInfo> placeDetails(String description, Place originCity){
        return searchPlaceService.searchPlaceDetails(description, originCity);
    }
}
