package com.finder.search.criteria;

public class ExtendPlaceSearchCriteria extends PlaceSearchCriteria {
    private String state;
    private String zipCode;

    public ExtendPlaceSearchCriteria(String country, String city, String state, String zipCode) {
        super(country, city);
        this.state = state;
        this.zipCode = zipCode;
    }

    public String getState() {
        return state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
