package com.finder.search.criteria;

public class PlaceSearchCriteria implements SearchCriteria{
    private String country;
    private String city;

    public PlaceSearchCriteria(String country, String city) {
        this.country = country;
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {

        return country;
    }

    public String getCity() {
        return city;
    }
}
