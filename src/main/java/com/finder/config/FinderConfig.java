package com.finder.config;

import com.finder.search.SearchFactory;
import com.finder.services.SearchCityService;
import com.finder.services.SearchPlaceService;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class FinderConfig {
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public SearchPlaceService searchPlaceService(){
        return new SearchPlaceService();
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public SearchCityService searchCityService(){
        return new SearchCityService();
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public SearchFactory searchFactory(SearchCityService searchCityService, SearchPlaceService searchPlaceService){
        return new SearchFactory(searchCityService, searchPlaceService);
    }
}
