package com.finder.errors;

public class AmbiguousCityException extends FinderRuntimeException{
    public AmbiguousCityException(String desc){
        super(desc);
    }
}
