package com.finder.errors;

public class FinderRuntimeException extends RuntimeException{
    private String desc = "";
    public FinderRuntimeException(String desc){
        super();
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
