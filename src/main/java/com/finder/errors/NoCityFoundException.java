package com.finder.errors;

public class NoCityFoundException extends FinderRuntimeException {
    public NoCityFoundException(String desc) {
        super(desc);
    }
}
