package com.finder.controllers.place.find;

import com.finder.errors.AmbiguousCityException;
import com.finder.errors.NoCityFoundException;
import com.finder.search.SearchFactory;
import facebook4j.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class PlaceFindController {
    private final static Logger LOGGER = Logger.getLogger(PlaceFindController.class);

    @Autowired
    SearchFactory searchFactory;

    @GetMapping(value = "/{country}/{city}/{description}")
    public List<PlaceInfo> getPlaceInfo(@PathVariable("country") String country, @PathVariable("city") String city, @RequestParam(value = "state", required = false) String cityState, @RequestParam(value = "zip", required = false) String zipCode, @PathVariable("description") String description) {
        LOGGER.info("Start looking for:" + "Country:[" + country + "]City:[" + city + "]Desc:[" + description + "]");
        try {
            Place originCity = searchFactory.cityDetails(country, city, Optional.ofNullable(cityState).orElse(""), Optional.ofNullable(zipCode).orElse(""));
            List<PlaceInfo> result = searchFactory.placeDetails(description, originCity);
            return result;
        }catch (AmbiguousCityException | NoCityFoundException e){
            LOGGER.error(e.getDesc());
            e.printStackTrace();
        }
        return null;
    }
}