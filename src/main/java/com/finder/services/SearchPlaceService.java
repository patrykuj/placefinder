package com.finder.services;

import com.finder.controllers.place.find.PlaceInfo;
import facebook4j.*;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SearchPlaceService {
    private final static Logger LOGGER = Logger.getLogger(SearchPlaceService.class);

    public SearchPlaceService() {
    }

    public List<PlaceInfo> searchPlaceDetails(String description, Place originCity){
        List<PlaceInfo> result = new ArrayList<>();
        Facebook facebook = new FacebookFactory().getInstance();
        try {
            facebook.setOAuthAccessToken(facebook.getOAuthAppAccessToken());
            Reading reading = new Reading();
            reading.fields("name", "id", "location");

            List<Place> places = facebook.searchPlaces(description, new GeoLocation(originCity.getLocation().getLatitude(), originCity.getLocation().getLongitude()), 10000, reading);
            result = places.stream().map(place -> new PlaceInfo(place.getName(), place.getLocation().getLatitude(), place.getLocation().getLongitude())).collect(Collectors.toList());


            for(Place place : places){
                LOGGER.debug(place.toString());
            }

        } catch (FacebookException e) {
            e.printStackTrace();
        }
        return result;
    }
}
