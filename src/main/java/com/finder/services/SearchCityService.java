package com.finder.services;

import com.finder.errors.AmbiguousCityException;
import com.finder.errors.NoCityFoundException;
import com.finder.search.criteria.ExtendPlaceSearchCriteria;
import com.finder.search.criteria.PlaceSearchCriteria;
import facebook4j.*;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.text.Normalizer;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class SearchCityService {
    private final static Logger LOGGER = Logger.getLogger(SearchCityService.class);

    private boolean extend = false;

    public SearchCityService() {}

    public Place searchCityDetails(PlaceSearchCriteria placeSearchCriteria) throws AmbiguousCityException, NoCityFoundException {
        extend = false;
        Reading reading = prepareReading();
        Place cityDetailsResult = executeSearchCommand(reading, placeSearchCriteria);
        return cityDetailsResult;
    }

    public Place searchCityDetails(ExtendPlaceSearchCriteria extendPlaceSearchCriteria) throws AmbiguousCityException, NoCityFoundException {
        extend = true;
        Reading reading = prepareReadingExtended(extendPlaceSearchCriteria);
        Place cityDetailsResult = executeSearchCommand(reading, extendPlaceSearchCriteria);
        return cityDetailsResult;
    }

    private Place executeSearchCommand(Reading reading, PlaceSearchCriteria placeSearchCriteria) throws AmbiguousCityException, NoCityFoundException {
        Place result = null;
        Facebook facebook = new FacebookFactory().getInstance();
        try {
            facebook.setOAuthAccessToken(facebook.getOAuthAppAccessToken());
            List<Place> searchOrigin = facebook.searchPlaces(placeSearchCriteria.getCity(), reading);
            result = validateSearch(searchOrigin, placeSearchCriteria.getCity(), placeSearchCriteria.getCountry(), "", "");
            return result;
        } catch (FacebookException e) {
            e.printStackTrace();
        }
        return result;
    }

    private Place executeSearchCommand(Reading reading, ExtendPlaceSearchCriteria placeSearchCriteria) throws AmbiguousCityException, NoCityFoundException{
        Place result = null;
        Facebook facebook = new FacebookFactory().getInstance();
        try {
            facebook.setOAuthAccessToken(facebook.getOAuthAppAccessToken());
            List<Place> searchOrigin = facebook.searchPlaces(placeSearchCriteria.getCity(), reading);
            result = validateSearch(searchOrigin, placeSearchCriteria.getCity(), placeSearchCriteria.getCountry(), placeSearchCriteria.getZipCode(), placeSearchCriteria.getState());
            return result;
        } catch (FacebookException e) {
            e.printStackTrace();
        }
        return result;
    }

    private Reading prepareReading(){
        Reading reading = new Reading();
        reading.fields("name", "id", "location");
        reading.limit(100);
        return reading;
    }

    private Reading prepareReadingExtended(ExtendPlaceSearchCriteria extendPlaceSearchCriteria) {
        Reading reading = prepareReading();
        reading.addParameter("state", Optional.ofNullable(extendPlaceSearchCriteria.getState()).orElse(""));
        reading.addParameter("zip", Optional.ofNullable(extendPlaceSearchCriteria.getZipCode()).orElse(""));
        return reading;
    }

    private Place validateSearch(List<Place> searchOrigin, String city, String country, String zip, String state) throws AmbiguousCityException, NoCityFoundException {
        LOGGER.debug("count:" + searchOrigin.size());
        for (Place placeOrigin : searchOrigin) {
            LOGGER.debug("Found: name:[" + placeOrigin.getName() + "]city:[" + placeOrigin.getLocation().getCity() + "]latitude:[" + placeOrigin.getLocation().getLatitude() + "]id:[" + placeOrigin.getId());
            LOGGER.debug(placeOrigin.toString());
            LOGGER.debug(placeOrigin.getLocation().toString());
        }
        Iterator<Place> it = searchOrigin.iterator();
        while (it.hasNext()){
            Place p = it.next();
            if(p.getId() == null || p.getLocation() == null)
                it.remove();
            if(p.getLocation().getCountry() == null || p.getLocation().getCity() == null)
                it.remove();
        }

        List<Place> result = searchOrigin.stream().filter(
                p -> {
                    LOGGER.debug("Found: name:[" + p.getName() + "]city:[" + p.getLocation().getCity() + "]latitude:[" + p.getLocation().getLatitude() + "]id:[" + p.getId());
                    LOGGER.debug(toEnglishAlphabet(p.getLocation().getCity()) + toEnglishAlphabet(p.getLocation().getCountry()));
                    LOGGER.debug(toEnglishAlphabet(p.getLocation().getCity()).equalsIgnoreCase(city) && toEnglishAlphabet(p.getLocation().getCountry()).equalsIgnoreCase(country) && toEnglishAlphabet(p.getName()).equalsIgnoreCase(city));
                    if(extend){
                        return city.equalsIgnoreCase(toEnglishAlphabet(p.getLocation().getCity())) && country.equalsIgnoreCase(toEnglishAlphabet(p.getLocation().getCountry())) && city.equalsIgnoreCase(toEnglishAlphabet(p.getName())) && ( zip.contains(toEnglishAlphabet(p.getLocation().getZip())) || state.equalsIgnoreCase(toEnglishAlphabet(p.getLocation().getState())) );
                    }else {
                        return city.equalsIgnoreCase(toEnglishAlphabet(p.getLocation().getCity())) && country.equalsIgnoreCase(toEnglishAlphabet(p.getLocation().getCountry())) && city.equalsIgnoreCase(toEnglishAlphabet(p.getName()));
                    }
                }).collect(Collectors.toList());
        if(result.size() == 0){
            throw new NoCityFoundException("City: " + city + " in country: " + country + " was not found");
        }
        else if(result.size() > 1){
            throw new AmbiguousCityException("Found " + result.size() + " cities with name " + city + " within country: " + country);
        }
        return result.get(0);
    }

    private String toEnglishAlphabet(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }
}
